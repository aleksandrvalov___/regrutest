<?php

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';

$model = new User();



$this->registerJs('

    $(document).ready(function(){
    
        $("#user-type").bind("change", function(){
        
            if($(this).val() == 0){
                $("#user-inn").parent().addClass("hidden");
				$("#user-organization").parent().addClass("hidden");
            }
            else {
                $("#addevent-intervalup").parent().parent().addClass("hidden");
            }
            
            if($(this).val() == 1){
                $("#user-organization").parent().addClass("hidden");
				$("#user-inn").parent().removeClass("hidden");
            }
     
			if($(this).val() == 2){
                $("#user-organization").parent().removeClass("hidden");
				$("#user-inn").parent().removeClass("hidden");
            }
            
        });
    
        $("#addevent-typemoderation").bind("change", function(){
    
            if($(this).val() == "moderatewithdelasy"){
                $("#addevent-moder_time").parent().parent().removeClass("hidden");
            }
            else{
                $("#addevent-moder_time").parent().parent().addClass("hidden");
            }
    
        });
    
    
    });

', \yii\web\View::POS_END);

?>
<div class="site-index">

  

    <div class="body-content">

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

					<?= $form->field($model, 'type')->dropDownList(User::$types,
                        [
                            'prompt' => 'Регистрируюсь как..',
                            'class'  => 'form-control'
                        ])->label("") ?>
				
                    <?= $form->field($model, 'fio')->textInput(['autofocus' => true])->label("ФИО") ?>

                    <?= $form->field($model, 'email')->label("Почта") ?>
					
                    <?= $form->field($model, 'inn', ['options'=>['class'=>'hidden']])->label("ИНН") ?>

					<?= $form->field($model, 'organization', ['options'=>['class'=>'hidden']])->label("Организация") ?>

                    <div class="form-group">
                        <?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
</div>
